package Ejercicio18;

import Ejercicio17.Electrodomestico;
import Ejercicio17.Lavadora;
import Ejercicio17.Television;

import java.util.ArrayList;

public class Ejecutable3 {

    public void videoJuegosSeries() {
        int videojuegosEntregados = 0;
        int videojuegoMasHoras = 0;
        int serieEntregadas = 0;
        int serieMasTemporadas = 0;
        ArrayList<VideoJuego> listaVideoJuegos = new ArrayList<VideoJuego>(5);
        ArrayList<Serie> listaSeries = new ArrayList<Serie>(5);

        VideoJuego videoJuego1 = new VideoJuego("Wow", 3789, "rol", "Blizzard");
        videoJuego1.entregar();
        listaVideoJuegos.add(videoJuego1);

        listaVideoJuegos.add(new VideoJuego("GTA", 125, "Accion aventura", "RockStart"));
        listaVideoJuegos.add(new VideoJuego("lol", 4850, "rol", "RIOT games"));
        VideoJuego videoJuego2 = new VideoJuego("Resident Evil", 90, "Accion suspenso", "Capcon");
        videoJuego2.entregar();
        listaVideoJuegos.add(videoJuego2);
        listaVideoJuegos.add(new VideoJuego("Mario", 24, "Aventura", "Nintendo"));
        listaSeries.add(new Serie("Juego de tronos", "David Benioff , D. B. Weiss"));
        listaSeries.add(new Serie("La casa de papel", "Alex pina"));
        Serie serie1 = new Serie("La casa de papel 2 ", 5, "Accion", "Alex pina");
        serie1.entregar();
        listaSeries.add(serie1);
        listaSeries.add(new Serie("vikings", 6, "Aventura Drama", "Michael Hirst"));
        listaSeries.add(new Serie("Stranger things", 3, "Misterio Horror", "Matt Duffer, Ross Duffer"));

        System.out.println("----------------------------Juegos Devueltos--------------------------------------");
        for (VideoJuego videojuego : listaVideoJuegos) {
            if (videojuego.entregado != false) {
                videojuegosEntregados += 1;
                System.out.println("Video juego devuelto: ");
                System.out.println(videojuego.toString());
            }
            if (videojuego.horasEstimadas > videojuegoMasHoras) {
                videojuegoMasHoras = videojuego.horasEstimadas;
            }
        }
        System.out.println("----------------------------Series Devueltas--------------------------------------");
        for (Serie serie : listaSeries) {
            if (serie.entregado != false) {
                serieEntregadas += 1;
                System.out.println("serie devuelta: ");
                System.out.println(serie.toString());
            }
            if (serie.numeoTemporadas > serieMasTemporadas) {
                serieMasTemporadas = serie.numeoTemporadas;
            }
        }
        System.out.println("----------------------------Juegos Y series Entregadas--------------------------------------");
        System.out.println(" El numero total de juegos entregados es de " + videojuegosEntregados);
        System.out.println(" El numero total de series entregadas es de " + serieEntregadas);

        System.out.println("----------------------------Juego Mas largo--------------------------------------");
        for (VideoJuego videojuego : listaVideoJuegos) {
            if (videojuego.horasEstimadas == videojuegoMasHoras) {
                System.out.println(" El videojuego con mas horas estimadas es: ");
                System.out.println(videojuego.toString());
            }
        }
        System.out.println("----------------------------Serie Mas larga--------------------------------------");
        for (Serie serie : listaSeries) {
            if (serie.numeoTemporadas == serieMasTemporadas) {
                System.out.println(" La serie con mas temporadas es: ");
                System.out.println(serie.toString());
            }
        }
        System.out.println("----------------------------Fin--------------------------------------");

    }
}
