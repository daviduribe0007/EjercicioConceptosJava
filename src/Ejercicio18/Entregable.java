package Ejercicio18;

public interface Entregable {

    boolean entregar();

    boolean devolver();

    boolean isEntregado();

    void compareTo(Object a);


}
