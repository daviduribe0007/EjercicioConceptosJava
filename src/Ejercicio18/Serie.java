package Ejercicio18;

public class Serie implements Entregable {
    String titulo;
    int numeoTemporadas = 3;
    boolean entregado = false;
    String genero;
    String creador;

    public Serie() {
    }

    public Serie(String titulo, String creador) {
        this.titulo = titulo;
        this.creador = creador;
    }

    public Serie(String titulo, int numeoTemporadas, String genero, String creador) {
        this.titulo = titulo;
        this.numeoTemporadas = numeoTemporadas;
        this.genero = genero;
        this.creador = creador;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getNumeoTemporadas() {
        return numeoTemporadas;
    }

    public void setNumeoTemporadas(int numeoTemporadas) {
        this.numeoTemporadas = numeoTemporadas;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    @Override
    public String toString() {
        return "{" +
                "Titulo: " + this.titulo +
                " Numero de temporadas: " + this.numeoTemporadas +
                " Genero: " + this.genero +
                " Creador: " + this.creador +
                "}";
    }

    @Override
    public boolean entregar() {
        return this.entregado = true;
    }

    @Override
    public boolean devolver() {
        return this.entregado = false;
    }

    @Override
    public boolean isEntregado() {
        return this.entregado;
    }

    @Override
    public void compareTo(Object a) {

    }
}
