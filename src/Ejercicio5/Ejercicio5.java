package Ejercicio5;

public class Ejercicio5 {

    public void numerosImpares() {
        int numero = 0;
        do {
            if (numero % 2 != 0) {
                System.out.println(numero);
            }
            numero++;

        } while (numero < 100);
    }
}
