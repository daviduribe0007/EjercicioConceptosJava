package Ejercicio16;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

public class Persona {
    private String nombre = "";
    private int edad = 0;
    private String DNI = "";
    private char sexo = 'H';
    private double peso = 0;
    private double altura = 0;

    public Persona() {
        this.DNI = generaDNI();
        comprobarSexo(this.sexo);
    }

    public Persona(String nombre, int edad, char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.DNI = generaDNI();
        comprobarSexo(this.sexo);
    }

    public Persona(String nombre, int edad, char sexo, double peso, double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.DNI = generaDNI();
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
        comprobarSexo(this.sexo);
    }

    public int calcularIMC() {
        double pesoIdeal;
        pesoIdeal = this.peso / Math.pow(this.altura, 2);
        if (pesoIdeal < 20) {
            return -1;
        } else if (pesoIdeal <= 25) {
            return 0;
        } else {
            return 1;
        }
    }

    public boolean esMayorDeEdad() {
        if (this.edad < 18) {
            return false;
        } else {
            return true;
        }
    }

    private void comprobarSexo(char sexo) {
        if (sexo == 'M') {
            this.sexo = sexo;
        } else {
            this.sexo = 'H';
        }
    }

    @Override
    public String toString() {
        String data = ("Nombre: " + this.nombre + " Edad: " + this.edad + " DNI: " + this.DNI
                + " Peso: " + this.peso + " Altura: " + this.altura);
        return data;
    }

    private String generaDNI() {
        Long numero = Math.round(Math.random() * 100000000);
        String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
        long resto = numero % 23;
        char letra = letras.charAt((int) resto);
        return numero + "-" + letra;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
}
