package Ejercicio16;

import java.util.Scanner;

public class Ejecutable {

    public void persona() {
        Scanner scan = new Scanner(System.in);
        String nombre;
        int edad = 0;
        char sexo = ' ';
        double peso = 0;
        double altura = 0;
        System.out.println("Ingrese el nombre");
        nombre = scan.nextLine();
        System.out.println("Ingrese la edad");
        try {
            edad = Integer.parseInt(scan.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Ingrese un numero");
            persona();
        }
        System.out.println("Ingrese sexo H. para hombre M. para mujer");
        try {

            sexo = scan.nextLine().charAt(0);

            if (sexo != 'H' || sexo != 'M' ) {
                System.out.println("Solo H y M son valores validos");
                persona();
            }
        } catch (NumberFormatException e) {
            System.out.println("Ingrese solo la letra en mayuscula");
            persona();
        }
        System.out.println("Ingrese el peso en kilos");
        try {
            peso = Double.parseDouble(scan.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Ingrese el peso en numeros");
            persona();
        }
        System.out.println("Ingrese la altura en metros");
        try {
            altura = Double.parseDouble(scan.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Ingrese la altura en numeros");
            persona();
        }
        Persona persona1 = new Persona(nombre, edad, sexo, peso, altura);
        Persona persona2 = new Persona(nombre, edad, sexo);
        Persona persona3 = new Persona();
        persona3.setNombre(nombre);
        persona3.setEdad(edad);
        persona3.setSexo(sexo);
        persona3.setPeso(peso);
        persona3.setAltura(altura);



        System.out.println(persona1.toString());
        System.out.println(persona2.toString());
        System.out.println(persona3.toString());
        mostrarEstado(persona1.calcularIMC(), " 1 ");
        mostrarEstado(persona2.calcularIMC(), " 2 ");
        mostrarEstado(persona3.calcularIMC(), " 3 ");
        comprobarEdad(persona1.esMayorDeEdad()," 1 ");
        comprobarEdad(persona2.esMayorDeEdad()," 2 ");
        comprobarEdad(persona3.esMayorDeEdad()," 3 ");

    }

    private void mostrarEstado(int indice, String persona) {
        if (indice == -1) {
            System.out.println("La persona " + persona + " esta en peso ideal ");
        } else if (indice == 0) {
            System.out.println("La persona " + persona + " esta por debajo de su peso ideal");
        } else {
            System.out.println("La persona " + persona + " tiene sobre peso");
        }
    }

    private void comprobarEdad(boolean edad, String persona) {
        if (edad) {
            System.out.println("La persona " + persona + " es mayor de edad");
        } else {
            System.out.println("La persona " + persona + " es menor de edad");
        }
    }


}
