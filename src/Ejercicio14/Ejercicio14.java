package Ejercicio14;

import java.util.Scanner;

public class Ejercicio14 {

    public void numerosHastaElMil() {
        int numero = 0;
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese el numero desde el cual desea iniciar el conteo a mil");
        try {
            numero = Integer.parseInt(scan.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Ingrese un numero");
        }

        if (numero > 1000) {
            System.out.println("El numero debe ser menor a 1000");
        } else {
            do {
                System.out.println(numero);
                numero += 2;
            } while (numero <= 1000);
        }
    }
}
