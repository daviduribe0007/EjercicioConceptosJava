package Ejercicio3;


import java.util.Scanner;

public class Ejercicio3 {

    public void areaCirculo() {
        double area;
        double radio = 0;
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese el radio del circulo");
        try {
            radio = Double.parseDouble(scan.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Ingrese un numero");
        }
        area = Math.PI * Math.pow(radio, 2);
        System.out.println("El area del circulo es igua a: " + String.format("%.2f", area));
    }
}
