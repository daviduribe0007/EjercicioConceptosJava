package Ejercicio17;

public class Electrodomestico {

    double precioBase = 100;
    String color = "Blanco";
    char consumoEnergetico = 'F';
    double peso = 5;
    double precioFinal = 0;

    public Electrodomestico() {
        comprobarConsumoEnergetico(this.consumoEnergetico);
        precioFinal();
    }

    public Electrodomestico(double precioBase, double peso) {
        this.precioBase = precioBase;
        this.peso = peso;
        comprobarConsumoEnergetico(this.consumoEnergetico);
        precioFinal();
    }

    public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
        this.precioBase = precioBase;
        this.color = color;
        this.consumoEnergetico = consumoEnergetico;
        this.peso = peso;
        comprobarConsumoEnergetico(this.consumoEnergetico);
        precioFinal();
    }

    public double getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(double precioBase) {
        this.precioBase = precioBase;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public char getConsumoEnergetico() {
        return consumoEnergetico;
    }

    public void setConsumoEnergetico(char consumoEnergetico) {
        this.consumoEnergetico = consumoEnergetico;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getPrecioFinal() {
        return precioFinal;
    }

    private void comprobarConsumoEnergetico(char letra) {
        if (letra != 'A' && letra != 'B' && letra != 'C' &&
                letra != 'D' && letra != 'E' && letra != 'F') {
            this.consumoEnergetico = 'F';
        } else {
            this.consumoEnergetico = letra;
        }
    }

    private void comprobarColor(String color) {
        if (color != "blanco" && color != "negro" && color != "rojo" &&
                color != "azul" && color != "gris") {
            this.color = "blanco";
        } else {
            this.color = color;
        }
    }

    private void precioFinal() {
        if (this.consumoEnergetico == 'A') {
            this.precioFinal += 100 + this.precioBase;
        } else if (this.consumoEnergetico == 'B') {
            this.precioFinal += 80 + this.precioBase;
        } else if (this.consumoEnergetico == 'C') {
            this.precioFinal += 60 + this.precioBase;
        } else if (this.consumoEnergetico == 'D') {
            this.precioFinal += 50 + this.precioBase;
        } else if (this.consumoEnergetico == 'E') {
            this.precioFinal += 30 + this.precioBase;
        } else if (this.consumoEnergetico == 'F') {
            this.precioFinal += 10 + this.precioBase;
        }
        if (this.peso <= 19) {
            this.precioFinal += 10;
        } else if (this.peso <= 49) {
            this.precioFinal += 50;
        } else if (this.peso <= 79) {
            this.precioFinal += 80;
        } else {
            this.precioFinal += 100;
        }
    }


}
