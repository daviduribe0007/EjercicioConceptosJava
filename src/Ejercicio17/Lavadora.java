package Ejercicio17;

public class Lavadora extends Electrodomestico {
    double carga = 5;

    public Lavadora() {
        precioFinal();
    }

    public Lavadora(double precioBase, double peso) {
        super(precioBase, peso);
        precioFinal();
    }

    public Lavadora(double precioBase, String color, char consumoEnergetico, double peso, double carga) {
        super(precioBase, color, consumoEnergetico, peso);
        this.carga = carga;
        precioFinal();
    }

    public double getCarga() {
        return carga;
    }

    private void precioFinal() {
        if (this.carga > 30) {
            super.precioFinal += 50;
        }

    }
}
