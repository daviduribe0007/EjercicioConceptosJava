package Ejercicio17;

public class Television extends Electrodomestico {

    double resolucion = 20;
    boolean sintonizadorTDT = false;

    public Television() {

        precioFinal();
    }

    public Television(double precioBase, double peso) {
        super(precioBase, peso);
        precioFinal();
    }

    public Television(double precioBase, String color, char consumoEnergetico, double peso, double resolucion, boolean sintonizadorTDT) {

        super(precioBase, color, consumoEnergetico, peso);
        this.resolucion = resolucion;
        this.sintonizadorTDT = sintonizadorTDT;
        precioFinal();
    }

    public double getResolucion() {
        return resolucion;
    }

    public boolean isSintonizadorTDT() {
        return sintonizadorTDT;
    }

    private void precioFinal() {

        if (this.resolucion > 40) {
            super.precioFinal *= 1.3;
        }
        if (this.sintonizadorTDT == true) {
            super.precioFinal += 50;
        }
    }
}
