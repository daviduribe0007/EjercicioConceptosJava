package Ejercicio17;

import java.util.ArrayList;

public class Ejecutable2 {


    public void ejecutables() {
        double precioTvs = 0;
        double precioLavadora = 0;
        double precioElectrodomesticos = 0;
        ArrayList<Electrodomestico> listaElectrodomesticos = new ArrayList<Electrodomestico>(10);
        listaElectrodomesticos.add(new Television(500, "blanco", 'A', 50, 40, true));
        listaElectrodomesticos.add(new Television(500, "blanco", 'E', 50, 40, true));
        listaElectrodomesticos.add(new Television(200, "negro", 'F', 20, 20, false));
        listaElectrodomesticos.add(new Television(600, "azul", 'E', 30, 49, false));
        listaElectrodomesticos.add(new Television(1000, "gris", 'A', 50, 80, true));
        listaElectrodomesticos.add(new Lavadora(1000, "gris", 'C', 98, 32));
        listaElectrodomesticos.add(new Lavadora(1000, "blanco", 'D', 30, 59));
        listaElectrodomesticos.add(new Lavadora(1000, "gris", 'A', 15, 31));
        listaElectrodomesticos.add(new Electrodomestico(800, "azul", 'D', 35));
        listaElectrodomesticos.add(new Electrodomestico(1000, "gris", 'A', 20));

        for (Electrodomestico electrodomestico : listaElectrodomesticos) {
            precioElectrodomesticos += electrodomestico.precioFinal;
            System.out.println(precioElectrodomesticos);
            if (electrodomestico instanceof Television) {
                precioTvs += electrodomestico.precioFinal;
            } else if (electrodomestico instanceof Lavadora) {
                precioLavadora += electrodomestico.precioFinal;
            }
        }
        System.out.println("El precio de los tvs es de :" + precioTvs);
        System.out.println("El precio de las lavadoras es de: " + precioLavadora);
        System.out.println("El precio de los electrodomesticos es de: " + precioElectrodomesticos);


    }

}
