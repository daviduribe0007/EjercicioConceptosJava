package Ejercicio2;

import Ejercicio1.Ejercicio1;

import java.util.Scanner;

public class Ejercicio2 {

    public void numeros() {
        int numero1 = 0;
        int numero2 = 0;
        Scanner scan = new Scanner(System.in);
        System.out.println("Bienvenido, ingrese el primer numero que desea comparar: ");
        try {
            numero1 = Integer.parseInt(scan.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Ingrese un numero");
        }
        System.out.println("Bienvenido, ingrese el segundo numero que desea comparar:");
        try {
            numero2 = Integer.parseInt(scan.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Ingrese un numero");
        }
        Ejercicio1 ejercicio1 = new Ejercicio1();
        ejercicio1.numeros(numero1, numero2);
    }
}
