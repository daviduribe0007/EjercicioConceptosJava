package Ejercicio7;

import java.util.Scanner;

public class Ejercicio7 {

    public void numeroMayorCero() {
        boolean salir = true;
        Scanner scan = new Scanner(System.in);
        int numero = 0;
        do {
            System.out.println("ingrese el numero mayor a 0 : ");
            try {
                numero = Integer.parseInt(scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Ingrese un numero");
            }

            if (numero >= 0) {
                salir = false;
            }
        } while (salir);
        System.out.println("EL numero " + numero + " es mayor o igual a 0");
    }
}
