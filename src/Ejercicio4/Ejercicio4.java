package Ejercicio4;

import java.util.Scanner;

public class Ejercicio4 {

    public void calcularIva() {

        final double iva = 1.21;
        double precioConIva = 0;
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese el valor del producto al cual le quiere calcular el iva: ");
        try {
            precioConIva = Double.parseDouble(scan.nextLine()) * iva;
        } catch (NumberFormatException e) {
            System.out.println("Ingrese un numero");
        }

        System.out.println("El precio total del producto con iva es de: " + precioConIva);
    }

}
