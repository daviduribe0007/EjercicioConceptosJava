package Ejercicio1;

public class Ejercicio1 {

    public void numeros(int numero1, int numero2) {
        if (numero1 < numero2) {
            System.out.println("El numero " + numero1 + " es mayor que " + numero2);
        } else if (numero1 == numero2) {
            System.out.println("Los dos numeros " + numero1 + " y " + numero2 + " son iguales");
        } else {
            System.out.println("El numero " + numero2 + " es mayor que " + numero1);
        }

    }

}
