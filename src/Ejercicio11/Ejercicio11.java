package Ejercicio11;

import java.util.Scanner;

public class Ejercicio11 {

    public void contarLetras() {
        String texto;
        int longitud;
        int a = 0;
        int e = 0;
        int i = 0;
        int o = 0;
        int u = 0;
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese una frase ");
        texto = scan.nextLine();

        longitud = texto.length();
        for (int j = 0; j < texto.length(); j++) {
            if (texto.charAt(j) == 'a') {
                a++;
            } else if (texto.charAt(j) == 'e') {
                e++;
            } else if (texto.charAt(j) == 'i') {
                i++;
            } else if (texto.charAt(j) == 'o') {
                o++;
            } else if (texto.charAt(j) == 'u') {
                u++;
            }
        }

        System.out.println("El total de caracteres de la frase es: " + longitud);
        System.out.println("La cantidad de letras a es igual a: " + a);
        System.out.println("La cantidad de letras e es igual a: " + e);
        System.out.println("La cantidad de letras i es igual a: " + i);
        System.out.println("La cantidad de letras o es igual a: " + o);
        System.out.println("La cantidad de letras u es igual a: " + u);


    }
}
