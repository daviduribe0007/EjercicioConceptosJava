package Ejercicio13;

import java.time.LocalDate;
import java.time.LocalTime;

public class Ejercicio13 {
    public void horaActual(){
        LocalDate fecha = LocalDate.now();
        LocalTime hora = LocalTime.now();
        System.out.println(fecha + " : " + hora);
    }
}
