package com.company;

import Ejercicio1.Ejercicio1;
import Ejercicio10.Ejercicio10;
import Ejercicio11.Ejercicio11;
import Ejercicio12.Ejercicio12;
import Ejercicio13.Ejercicio13;
import Ejercicio14.Ejercicio14;
import Ejercicio15.Ejercicio15;
import Ejercicio16.Ejecutable;
import Ejercicio17.Ejecutable2;
import Ejercicio18.Ejecutable3;
import Ejercicio2.Ejercicio2;
import Ejercicio3.Ejercicio3;
import Ejercicio4.Ejercicio4;
import Ejercicio5.Ejercicio5;
import Ejercicio6.Ejercicio6;
import Ejercicio7.Ejercicio7;
import Ejercicio8.Ejercicio8;
import Ejercicio9.Ejercicio9;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // descomentar el ejercicio que quiere probar
        int opcion = 0;
        boolean bandera = true;

        Scanner scan = new Scanner(System.in);
        do {
            System.out.println(" -------------------------------------------------------");
            System.out.println(" ----Ingrese el numero de la opcion que desea probar----");
            System.out.println("1. Ejercicio1");
            System.out.println("2. Ejercicio2");
            System.out.println("3. Ejercicio3");
            System.out.println("4. Ejercicio4");
            System.out.println("5. Ejercicio5");
            System.out.println("6. Ejercicio6");
            System.out.println("7. Ejercicio7");
            System.out.println("8. Ejercicio8");
            System.out.println("9. Ejercicio9");
            System.out.println("10. Ejercicio10");
            System.out.println("11. Ejercicio11");
            System.out.println("12. Ejercicio12");
            System.out.println("13. Ejercicio13");
            System.out.println("14. Ejercicio14");
            System.out.println("15. Ejercicio15");
            System.out.println("16. Ejercicio16");
            System.out.println("17. Ejercicio17");
            System.out.println("18. Ejercicio18");
            System.out.println("0. Para salir");


            try {
                opcion = Integer.parseInt(scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("Ingrese un numero");
            }
            switch (opcion) {
                case 1:
                    Ejercicio1 ejemplo1 = new Ejercicio1();
                    ejemplo1.numeros(1, 2);
                    break;
                case 2:
                    Ejercicio2 ejercicio2 = new Ejercicio2();
                    ejercicio2.numeros();
                    break;
                case 3:
                    Ejercicio3 ejercicio3 = new Ejercicio3();
                    ejercicio3.areaCirculo();
                    break;
                case 4:
                    Ejercicio4 ejercicio4 = new Ejercicio4();
                    ejercicio4.calcularIva();
                    break;
                case 5:
                    Ejercicio5 ejercicio5 = new Ejercicio5();
                    ejercicio5.numerosImpares();
                    break;
                case 6:
                    Ejercicio6 ejercicio6 = new Ejercicio6();
                    ejercicio6.numerosImpares();
                    break;
                case 7:
                    Ejercicio7 ejercicio7 = new Ejercicio7();
                    ejercicio7.numeroMayorCero();
                    break;
                case 8:
                    Ejercicio8 ejercicio8 = new Ejercicio8();
                    ejercicio8.diaSemana();
                    break;
                case 9:
                    Ejercicio9 ejercicio9 = new Ejercicio9();
                    ejercicio9.reemplazaLetras();
                    break;
                case 10:
                    Ejercicio10 ejercicio10 = new Ejercicio10();
                    ejercicio10.eliminarEspacios();
                    break;
                case 11:
                    Ejercicio11 ejercicio11 = new Ejercicio11();
                    ejercicio11.contarLetras();
                    break;
                case 12:
                    Ejercicio12 ejercicio12 = new Ejercicio12();
                    ejercicio12.fracesDiferentes();
                    break;
                case 13:
                    Ejercicio13 ejercicio13 = new Ejercicio13();
                    ejercicio13.horaActual();
                    break;
                case 14:
                    Ejercicio14 ejercicio14 = new Ejercicio14();
                    ejercicio14.numerosHastaElMil();
                    break;
                case 15:
                    Ejercicio15 ejercicio15 = new Ejercicio15();
                    ejercicio15.menu();
                    break;
                case 16:
                    Ejecutable ejercicio16 = new Ejecutable();
                    ejercicio16.persona();

                    break;
                case 17:
                    Ejecutable2 ejercicio17 = new Ejecutable2();
                    ejercicio17.ejecutables();
                    break;
                case 18:
                    Ejecutable3 ejercicio18 = new Ejecutable3();
                    ejercicio18.videoJuegosSeries();
                    break;
                default:

                    bandera = false;
                    break;


            }


        } while (bandera);

    }
}
