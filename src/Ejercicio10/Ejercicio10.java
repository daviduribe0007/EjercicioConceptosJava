package Ejercicio10;

import java.util.Scanner;

public class Ejercicio10 {

    public void eliminarEspacios() {
        String texto;
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese una frase ");
        texto = scan.nextLine();
        System.out.println(texto.replaceAll("\\s", ""));
    }
}

